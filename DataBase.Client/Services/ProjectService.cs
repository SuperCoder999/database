using System.Threading.Tasks;
using System.Collections.Generic;
using DataBase.Common.DTO.Project;

namespace DataBase.Client.Services
{
    internal sealed class ProjectService : AbstarctService
    {
        public ProjectService() : base("projects") { }

        public async Task<IEnumerable<ProjectWithTasksCountDTO>> GetTasksCountByUserId(int userId)
        {
            return await _api.Get<IEnumerable<ProjectWithTasksCountDTO>>($"with-tasks-count/{userId}");
        }

        public async Task<ProjectAdditionalInfoDTO> GetAdditionalInfo(int id)
        {
            return await _api.Get<ProjectAdditionalInfoDTO>($"{id}/additional-info");
        }
    }
}
