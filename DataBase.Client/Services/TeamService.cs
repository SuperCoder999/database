using System.Threading.Tasks;
using System.Collections.Generic;
using DataBase.Common.DTO.Team;

namespace DataBase.Client.Services
{
    internal sealed class TeamService : AbstarctService
    {
        public TeamService() : base("teams") { }

        public async Task<IEnumerable<TeamShortDTO>> GetIdsNamesUsers()
        {
            return await _api.Get<IEnumerable<TeamShortDTO>>("ids-names-users");
        }
    }
}
