using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataBase.Common;
using DataBase.Client.Commands;

namespace DataBase.Client.ConsoleInterface
{
    public sealed class ConsoleMenu
    {
        private readonly List<ICommand> commands;
        private readonly CommandRenderer renderer;
        private readonly CommandExecutor executor;

        public ConsoleMenu()
        {
            commands = FindCommands();
            renderer = new CommandRenderer(commands);
            executor = new CommandExecutor(commands);
        }

        public async Task Run()
        {
            Render();
            string choice = AskCommand();
            bool shouldStop = await HandleChoice(choice);
            ResetCommands();

            if (!shouldStop)
            {
                await Run();
            }
        }

        private void Render()
        {
            Console.WriteLine("Select command by typing it's number:");
            renderer.RenderCommandsListInConsole();
            Console.WriteLine($"(Enter '{Settings.exitCommand}' to exit)");
            Console.Write(":");
        }

        private string AskCommand()
        {
            return Console.ReadLine();
        }

        private async Task<bool> HandleChoice(string choice)
        {
            if (choice == Settings.exitCommand)
            {
                return true;
            }

            Console.WriteLine();

            try
            {
                int index = Convert.ToInt32(choice);
                await executor.ExecuteCommandByIndex(index);
            }
            catch (FormatException)
            {
                Console.WriteLine($"Enter a valid number or '{Settings.exitCommand}' to exit");
            }
            catch (Exception exc)
            {
                Console.WriteLine($"Error: {exc.Message}");
            }

            Console.WriteLine();
            return false;
        }

        private static List<ICommand> FindCommands()
        {
            return AppDomain
                .CurrentDomain
                .GetAssemblies()
                .SelectMany(a => a.GetTypes())
                .Where(t => Utils.GetAttribute<CommandAttribute>(t) != null)
                .Select(t => (ICommand)Activator.CreateInstance(t))
                .OrderBy(cmd => cmd.Index)
                .ToList();
        }

        private void ResetCommands()
        {
            foreach (ICommand command in commands)
            {
                command.Reset();
            }
        }
    }
}
