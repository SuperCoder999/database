﻿using System.Threading.Tasks;
using DataBase.Client.ConsoleInterface;

namespace DataBase.Client
{
    public sealed class Program
    {
        public static async Task Main(string[] args)
        {
            ConsoleMenu app = new ConsoleMenu();
            await app.Run();
        }
    }
}
