using System;
using System.Threading.Tasks;

namespace DataBase.Client.API
{
    internal sealed class ApiClient : IDisposable
    {
        private readonly IApiProvider _provider;
        private readonly string _basicEndpoint = "";

        public ApiClient(IApiProvider provider)
        {
            _provider = provider;
        }

        public ApiClient(IApiProvider provider, string basicEndpoint)
        {
            _provider = provider;
            _basicEndpoint = basicEndpoint + "/";
        }

        public async Task<T> Get<T>()
        {
            return await _provider.Get<T>("");
        }

        public async Task<T> Get<T>(string endpoint)
        {
            return await _provider.Get<T>(_basicEndpoint + endpoint);
        }

        public void Dispose()
        {
            _provider.Dispose();
        }
    }
}
