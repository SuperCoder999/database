using System;
using System.Threading.Tasks;

namespace DataBase.Client.API
{
    public interface IApiProvider : IDisposable
    {
        Task<T> Get<T>(string endpoint);
    }
}
