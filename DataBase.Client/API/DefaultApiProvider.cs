#nullable enable

using System;
using System.Threading.Tasks;
using System.Net.Http;
using DataBase.Common;
using DataBase.Common.DTO;

namespace DataBase.Client.API
{
    internal sealed class DefaultApiProvider : IApiProvider
    {
        private static readonly string BaseUrl = "http://localhost:5000/api/";
        private readonly HttpClient client = new HttpClient();

        public DefaultApiProvider()
        {
            client.BaseAddress = new Uri(BaseUrl);
        }

        public async Task<T> Get<T>(string endpoint)
        {
            HttpResponseMessage response = await client.GetAsync(endpoint);
            string stringBody = await response.Content.ReadAsStringAsync();
            ThrowIfResponseNotOk(response, stringBody);
            T result = Utils.DeserializeJson<T>(stringBody);

            return result;
        }

        public void Dispose()
        {
            client.Dispose();
        }

        private void ThrowIfResponseNotOk(HttpResponseMessage response, string strBody)
        {
            if (!response.IsSuccessStatusCode)
            {
                HttpErrorDTO? error = Utils.DeserializeJson<HttpErrorDTO>(strBody);

                if (error.HasValue)
                {
                    throw new InvalidOperationException(error.Value.Error);
                }

                throw new InvalidOperationException(response.ReasonPhrase);
            }
        }
    }
}
