using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace DataBase.Client
{
    internal static class Settings
    {
        public static readonly string exitCommand = "exit";

        public readonly static ReadOnlyDictionary<string, string> commandTexts = new ReadOnlyDictionary<string, string>(
            new Dictionary<string, string> {
                { "getProjectsTasksByUserId", "Get projects with tasks count by user id" },
                { "getAssignedTasks", "Get tasks with short name assigned to certain user" },
                {
                    "getShortTasksFinishedInCurrentYearAndAssignedTo",
                    "Get short info about tasks that are finished in current year and assigned to certain user"
                },
                {
                    "getTeamsWithUsersOlder10Years",
                    "Get teams with users older than 10 years ordered by descending registration date"
                },
                {
                    "getUsersOrderedByFirstNameWithTasks",
                    "Get users ordered by first name with tasks ordered by name length"
                },
                { "getUserInfo", "Get user info by id" },
                { "getProjectInfo", "Get project info by id" },
            }
        );

        public readonly static ReadOnlyDictionary<string, string> parameterPrompts = new ReadOnlyDictionary<string, string>(
            new Dictionary<string, string> {
                { "userId", "Enter user id: " },
                { "projectId", "Enter project id: " },
            }
        );

        public readonly static ReadOnlyDictionary<string, string> parameterErrors = new ReadOnlyDictionary<string, string>(
            new Dictionary<string, string> {
                { "invalidNumber", "Enter a valid number" },
            }
        );
    }
}
