using System;

namespace DataBase.Client.Commands
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    internal sealed class CommandAttribute : Attribute
    {
        public int Index { get; }
        public string Text { get; }

        public CommandAttribute(int index, string textKey)
        {
            Index = index;
            Text = Settings.commandTexts[textKey];
        }

        public string GetMenuString()
        {
            return $"{Index}. {Text}";
        }
    }
}
