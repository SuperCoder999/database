using System;
using System.Threading.Tasks;
using DataBase.Common.DTO.Project;
using DataBase.Client.Services;

namespace DataBase.Client.Commands
{
    [Command(7, "getProjectInfo")]
    [UseParameter("projectId", "projectId", "invalidNumber")]
    internal sealed class GetProjectInfo : BaseCommand<GetProjectInfo>
    {
        private readonly ProjectService projectService = new ProjectService();

        public override async Task Invoke()
        {
            int projectId = GetParameterValue<int>("projectId");
            ProjectAdditionalInfoDTO info = await projectService.GetAdditionalInfo(projectId);

            string computedUsersCount = info.UsersCountInTeam.HasValue
                ? info.UsersCountInTeam.Value.ToString()
                : "Project doesn't meet the requirements";

            Console.WriteLine($"Project: {info.Project.Name}");
            Console.WriteLine($"Longest description task: {(info.LongestDescriptionTask.HasValue ? info.LongestDescriptionTask.Value.Name : "No")}");
            Console.WriteLine($"Shortest name task: {(info.ShortestNameTask.HasValue ? info.ShortestNameTask.Value.Name : "No")}");
            Console.WriteLine($"Users count in team: {computedUsersCount}");
        }
    }
}
