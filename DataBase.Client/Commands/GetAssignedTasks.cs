using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataBase.Common.DTO.Task;
using DataBase.Client.Services;

namespace DataBase.Client.Commands
{
    [Command(2, "getAssignedTasks")]
    [UseParameter("userId", "userId", "invalidNumber")]
    internal sealed class GetAssignedTasks : BaseCommand<GetAssignedTasks>
    {
        private readonly TaskService taskService = new TaskService();

        public override async Task Invoke()
        {
            int userId = GetParameterValue<int>("userId");
            IEnumerable<TaskDTO> tasks = await taskService.GetAssignedToWithShortName(userId);

            foreach (TaskDTO task in tasks)
            {
                Console.WriteLine($"Task '{task.Name}' with state '{task.State}'");
            }
        }
    }
}
