using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using DataBase.Common;

namespace DataBase.Client.Commands
{
    internal abstract class BaseCommand<TDerived> : ICommand where TDerived : class
    {
        public int Index { get => metadata.Index; }
        private readonly CommandAttribute metadata;
        private readonly List<UseParameterAttribute> parameters;

        public BaseCommand()
        {
            metadata = Utils.GetAttribute<TDerived, CommandAttribute>();
            parameters = Utils.GetAttributes<TDerived, UseParameterAttribute>();

            if (metadata == null)
            {
                throw new InvalidOperationException("Trying to create a command without Command attribute");
            }
        }

        protected T GetParameterValue<T>(string key) where T : struct
        {
            UseParameterAttribute parameter = SafelyFindParameter(key);

            return parameter.GetValue<T>();
        }

        protected T GetParameterValue<T>(string key, Func<string, T> converter)
        {
            UseParameterAttribute parameter = SafelyFindParameter(key);

            return parameter.GetValue<T>(converter);
        }

        private UseParameterAttribute SafelyFindParameter(string key)
        {
            UseParameterAttribute parameter = parameters.Find(p => p.Key == key);

            if (parameter == null)
            {
                throw new InvalidOperationException("Trying to get value of an unexisting parameter");
            }

            return parameter;
        }

        public override string ToString()
        {
            return metadata.GetMenuString();
        }

        public void Reset()
        {
            foreach (UseParameterAttribute parameter in parameters)
            {
                parameter.Reset();
            }
        }

        public abstract Task Invoke();
    }
}
