using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataBase.Common.DTO.Task;
using DataBase.Client.Services;

namespace DataBase.Client.Commands
{
    [Command(3, "getShortTasksFinishedInCurrentYearAndAssignedTo")]
    [UseParameter("userId", "userId", "invalidNumber")]
    internal sealed class GetShortTasksFinishedInCurrentYearAssignedTo
        : BaseCommand<GetShortTasksFinishedInCurrentYearAssignedTo>
    {
        private readonly TaskService taskService = new TaskService();

        public override async Task Invoke()
        {
            int userId = GetParameterValue<int>("userId");
            IEnumerable<TaskShortDTO> data = await taskService.GetShortFinishedInCurrentYearAssignedTo(userId);

            foreach (TaskShortDTO dto in data)
            {
                Console.WriteLine($"Id: {dto.Id}, Name: {dto.Name}");
            }
        }
    }
}
