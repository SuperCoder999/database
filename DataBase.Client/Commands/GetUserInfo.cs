using System;
using System.Threading.Tasks;
using DataBase.Common.DTO.User;
using DataBase.Client.Services;

namespace DataBase.Client.Commands
{
    [Command(6, "getUserInfo")]
    [UseParameter("userId", "userId", "invalidNumber")]
    internal sealed class GetUserInfo : BaseCommand<GetUserInfo>
    {
        private readonly UserService userService = new UserService();

        public override async Task Invoke()
        {
            int userId = GetParameterValue<int>("userId");
            UserAdditionalInfoDTO info = await userService.GetAdditionalInfo(userId);

            Console.WriteLine($"User: {info.User.FirstName} {info.User.LastName}");
            Console.WriteLine($"Last project: {(info.LastProject.HasValue ? info.LastProject.Value.Name : "No")}");
            Console.WriteLine($"Last project tasks count: {info.LastProjectTasksCount}");
            Console.WriteLine($"Unfinished tasks count: {info.UnfinishedTasksCount}");
            Console.WriteLine($"Longest task: {(info.LongestTask.HasValue ? info.LongestTask.Value.Name : "No")}");
        }
    }
}
