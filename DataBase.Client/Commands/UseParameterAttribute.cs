using System;

namespace DataBase.Client.Commands
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    internal sealed class UseParameterAttribute : Attribute
    {
        public string Key { get; }
        public string Prompt { get; }
        public string Error { get; }
        private string Value { get; set; }

        public UseParameterAttribute(string key, string promptKey, string errorKey)
        {
            Key = key;
            Prompt = Settings.parameterPrompts[promptKey];
            Error = Settings.parameterErrors[errorKey];
        }

        public UseParameterAttribute(string key, string promptKey) // When parameter is a plain string
        {
            Key = key;
            Prompt = Settings.parameterPrompts[promptKey];
            Error = "";
        }

        public T GetValue<T>(Func<string, T> converter)
        {
            try
            {
                return converter(FetchValue());
            }
            catch
            {
                Value = null;
                Console.WriteLine(Error);
                return GetValue<T>(converter);
            }
        }

        public T GetValue<T>() where T : struct
        {
            try
            {
                string typeName = typeof(T).Name;
                string convertMethodName = "To" + typeName;

                object boxedResult = typeof(Convert)
                    .GetMethod(convertMethodName, new Type[] { typeof(string) })
                    .Invoke(null, new object[] { FetchValue() });

                return (T)boxedResult;
            }
            catch
            {
                Value = null;
                Console.WriteLine(Error);
                return GetValue<T>();
            }
        }

        public void Reset()
        {
            Value = null;
        }

        private string FetchValue()
        {
            if (Value == null)
            {
                Value = AskValue();
            }

            return Value;
        }

        private string AskValue()
        {
            Console.Write(Prompt);
            return Console.ReadLine();
        }
    }
}
