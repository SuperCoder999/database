using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using DataBase.DAL.Entities;
using DataBase.BLL.Interfaces;
using DataBase.Common.DTO.User;

namespace DataBase.WebAPI.Controllers
{
    [ApiController]
    [Route("api/users")]
    public sealed class UserController : AbstractController<IUserService, User, UserDTO, CreateUserDTO, UpdateUserDTO>
    {
        public UserController(IUserService service) : base(service) { }

        [HttpGet("with-tasks")]
        public async Task<ActionResult<IEnumerable<UserWithTasksDTO>>> GetWithTasksOrderedByFirstName()
        {
            return Ok(await _service.GetWithTasksOrderedByFirstName());
        }

        [HttpGet("{id}/additional-info")]
        public async Task<ActionResult<UserAdditionalInfoDTO>> GetAdditionalInfo([FromRoute] int id)
        {
            return Ok(await _service.GetAdditionalInfo(id));
        }
    }
}
