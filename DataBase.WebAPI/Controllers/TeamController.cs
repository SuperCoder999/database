using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using DataBase.DAL.Entities;
using DataBase.BLL.Interfaces;
using DataBase.Common.DTO.Team;

namespace DataBase.WebAPI.Controllers
{
    [ApiController]
    [Route("api/teams")]
    public sealed class TeamController : AbstractController<ITeamService, Team, TeamDTO, CreateTeamDTO, UpdateTeamDTO>
    {
        public TeamController(ITeamService service) : base(service) { }

        [HttpGet("ids-names-users")]
        public async Task<ActionResult<IEnumerable<TeamShortDTO>>> GetShortWithUsersOlder10()
        {
            return Ok(await _service.GetShortWithUsersOlder10());
        }
    }
}
