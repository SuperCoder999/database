using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FluentValidation;
using DataBase.DAL.Entities;
using DataBase.BLL.Interfaces;

namespace DataBase.WebAPI.Controllers
{
    public abstract class AbstractController<TService, TEntity, TDTO, TCreateDTO, TUpdateDTO> : ControllerBase
        where TService : IService<TEntity, TDTO, TCreateDTO>
        where TEntity : class, IEntity, new()
        where TDTO : struct
        where TCreateDTO : struct
        where TUpdateDTO : struct
    {
        protected readonly TService _service;

        public AbstractController(TService service)
        {
            _service = service;
        }

        [HttpGet]
        public virtual async Task<ActionResult<IEnumerable<TDTO>>> Get()
        {
            return Ok(await _service.Get());
        }

        [HttpGet("{id}")]
        public virtual async Task<ActionResult<TDTO>> Get([FromRoute] int id)
        {
            return Ok(await _service.Get(id));
        }

        [HttpPost]
        public virtual async Task<ActionResult<TDTO>> Create([FromBody] TCreateDTO data)
        {
            return Created(nameof(TEntity), await _service.Create(data));
        }

        [HttpPatch("{id}")]
        public virtual async Task<ActionResult<TDTO>> Update([FromRoute] int id)
        {
            string stringBody = await ParseStringRequestBody();

            TEntity entity = await _service.GetRaw(id);
            entity = await _service.RepopulateUpdateRequest(stringBody, entity);

            return Ok(await _service.Update(id, entity));
        }

        [HttpDelete("{id}")]
        public virtual async Task<ActionResult> Delete([FromRoute] int id)
        {
            await _service.Delete(id);
            return NoContent();
        }

        protected virtual async Task<string> ParseStringRequestBody()
        {
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                return await reader.ReadToEndAsync();
            }
        }
    }
}
