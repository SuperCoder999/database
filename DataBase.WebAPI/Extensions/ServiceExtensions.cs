using System;
using dotenv.net;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using FluentValidation;
using FluentValidation.AspNetCore;
using DataBase.DAL.Context;
using DataBase.BLL.Interfaces;
using DataBase.BLL.Services;
using DataBase.BLL.MappingProfiles;
using DataBase.BLL.Validators.User;
using DataBase.BLL.Validators.Task;
using DataBase.BLL.Validators.Team;
using DataBase.BLL.Validators.Project;
using DataBase.Common.DTO.User;
using DataBase.Common.DTO.Task;
using DataBase.Common.DTO.Team;
using DataBase.Common.DTO.Project;
using DataBase.WebAPI.Filters;

namespace DataBase.WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void LoadEnvironment(this IServiceCollection services)
        {
            DotEnv.Load(new DotEnvOptions(false, new string[] { "../.env" }));
        }

        public static void AddDAL(this IServiceCollection services)
        {
            string migrationsAssembly = typeof(DatabaseContext).Assembly.GetName().FullName;

            services.AddDbContext<DatabaseContext>(opt =>
                opt.UseNpgsql(
                    Environment.GetEnvironmentVariable("DB_CONNECTION_STRING"),
                    opt => opt
                        .MigrationsAssembly(migrationsAssembly)
                        .UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery)
                ));
        }

        public static void AddMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(opt =>
            {
                opt.AddProfile<UserProfile>();
                opt.AddProfile<TaskProfile>();
                opt.AddProfile<TeamProfile>();
                opt.AddProfile<ProjectProfile>();
            });
        }

        public static void AddValidation(this IServiceCollection services)
        {
            services.AddSingleton<IValidator<CreateUserDTO>, CreateUserValidator>();
            services.AddSingleton<IValidator<UpdateUserDTO>, UpdateUserValidator>();

            services.AddSingleton<IValidator<CreateTaskDTO>, CreateTaskValidator>();
            services.AddSingleton<IValidator<UpdateTaskDTO>, UpdateTaskValidator>();

            services.AddSingleton<IValidator<CreateTeamDTO>, CreateTeamValidator>();
            services.AddSingleton<IValidator<UpdateTeamDTO>, UpdateTeamValidator>();

            services.AddSingleton<IValidator<CreateProjectDTO>, CreateProjectValidator>();
            services.AddSingleton<IValidator<UpdateProjectDTO>, UpdateProjectValidator>();
        }

        public static void AddBLL(this IServiceCollection services)
        {
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<ITaskService, TaskService>();
        }

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(opt =>
            {
                opt.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "DataBase API",
                    Version = "1.0.0"
                });
            });
        }

        public static void AddAPI(this IServiceCollection services)
        {
            services
                .AddControllers(opt => opt.Filters.Add(typeof(CustomExceptionFilterAttribute)))
                .AddNewtonsoftJson()
                .AddFluentValidation();
        }
    }
}
