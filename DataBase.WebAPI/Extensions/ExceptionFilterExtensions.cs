using System;
using System.Net;
using System.Linq;
using Newtonsoft.Json;
using FluentValidation;
using DataBase.Common.Exceptions;

namespace DataBase.WebAPI.Extensions
{
    public static class ExceptionFilterExtensions
    {
        public static (string, HttpStatusCode) ParseException(this Exception exception)
        {
            return exception switch
            {
                NotFoundException exc => (exc.Message, HttpStatusCode.NotFound),
                JsonException exc => (exc.Message, HttpStatusCode.BadRequest),
                ValidationException exc => (
                    string.Join(", ", exc.Errors.Select(err => err.ErrorMessage)),
                    HttpStatusCode.UnprocessableEntity
                ),
                Exception exc => (exc.Message, HttpStatusCode.InternalServerError),
            };
        }
    }
}
