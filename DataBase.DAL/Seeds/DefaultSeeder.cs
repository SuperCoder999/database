using System;
using System.Collections.Generic;
using Bogus;
using Bogus.DataSets;
using DataBase.DAL.Entities;
using DataBase.Common.Enums;

namespace DataBase.DAL.Seeds
{
    internal sealed class DefaultSeeder : ISeeder
    {
        public IEnumerable<User> GetUsers(int count, IEnumerable<Team> teams)
        {
            int userId = 1;

            Faker<User> faker = new Faker<User>()
                .RuleFor(u => u.Id, f => userId++)
                .RuleFor(u => u.Email, f => f.Internet.Email())
                .RuleFor(u => u.FirstName, f => f.Name.FirstName(Name.Gender.Male))
                .RuleFor(u => u.LastName, f => f.Name.LastName(Name.Gender.Male))
                .RuleFor(u => u.TeamId, f => f.PickRandom(teams).Id)
                .RuleFor(u => u.BirthDay, f => f.Date.Past(75, DateTime.UtcNow));

            return faker.Generate(count);
        }

        public IEnumerable<TaskModel> GetTasks(int count, IEnumerable<User> users, IEnumerable<Project> projects)
        {
            int taskId = 1;

            Faker<TaskModel> faker = new Faker<TaskModel>()
                .RuleFor(t => t.Id, f => taskId++)
                .RuleFor(t => t.Name, f => f.Lorem.Sentence(f.Random.Number(3, 10)))
                .RuleFor(t => t.Description, f => f.Lorem.Sentences(f.Random.Number(2, 10), " "))
                .RuleFor(t => t.ProjectId, f => f.PickRandom(projects).Id)
                .RuleFor(t => t.PerformerId, f => f.PickRandom(users).Id)
                .RuleFor(t => t.State, f => (TaskState)f.Random.Number(0, 3))
                .RuleFor(t => t.FinishedAt, f => f.Random.Number() == 0 ? f.Date.Recent(100, DateTime.UtcNow) : null);

            return faker.Generate(count);
        }

        public IEnumerable<Team> GetTeams(int count)
        {
            int teamId = 1;

            Faker<Team> faker = new Faker<Team>()
                .RuleFor(t => t.Id, f => teamId++)
                .RuleFor(t => t.Name, f => f.Company.CompanyName(0));

            return faker.Generate(count);
        }

        public IEnumerable<Project> GetProjects(int count, IEnumerable<User> users, IEnumerable<Team> teams)
        {
            int projectId = 1;

            Faker<Project> faker = new Faker<Project>()
                .RuleFor(p => p.Id, f => projectId++)
                .RuleFor(p => p.Name, f => f.Lorem.Sentence(f.Random.Number(3, 10)))
                .RuleFor(p => p.Description, f => f.Lorem.Sentences(f.Random.Number(2, 10), " "))
                .RuleFor(p => p.TeamId, f => f.PickRandom(teams).Id)
                .RuleFor(p => p.AuthorId, f => f.PickRandom(users).Id)
                .RuleFor(p => p.Deadline, f => f.Date.Future(3, DateTime.UtcNow));

            return faker.Generate(count);
        }
    }
}
