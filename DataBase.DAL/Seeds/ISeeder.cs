using System.Collections.Generic;
using DataBase.DAL.Entities;

namespace DataBase.DAL.Seeds
{
    internal interface ISeeder
    {
        IEnumerable<User> GetUsers(int count, IEnumerable<Team> teams);
        IEnumerable<TaskModel> GetTasks(int count, IEnumerable<User> users, IEnumerable<Project> projects);
        IEnumerable<Team> GetTeams(int count);
        IEnumerable<Project> GetProjects(int count, IEnumerable<User> users, IEnumerable<Team> teams);
    }
}
