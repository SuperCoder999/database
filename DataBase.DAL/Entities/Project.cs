using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataBase.DAL.Entities
{
    public sealed class Project : IEntity
    {
        public Project()
        {
            CreatedAt = DateTime.UtcNow;
            Tasks = new List<TaskModel>();
        }

        public int Id { get; set; }
        public int? AuthorId { get; set; }
        public User Author { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }

        [Required]
        [MinLength(5)]
        [MaxLength(200)]
        public string Name { get; set; }

        [MinLength(2)]
        [MaxLength(2000)]
        [DataType("text")]
        public string Description { get; set; }

        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public IEnumerable<TaskModel> Tasks { get; set; }
    }
}
