using System;
using System.ComponentModel.DataAnnotations;
using DataBase.Common.Enums;

namespace DataBase.DAL.Entities
{
    // Conflicts with System.Threading.Tasks.Task
    public sealed class TaskModel : IEntity
    {
        public TaskModel()
        {
            CreatedAt = DateTime.UtcNow;
        }

        public int Id { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public int? PerformerId { get; set; }
        public User Performer { get; set; }

        [Required]
        [MinLength(5)]
        [MaxLength(200)]
        public string Name { get; set; }

        [MinLength(2)]
        [MaxLength(700)]
        [DataType("text")]
        public string Description { get; set; }

        [EnumDataType(typeof(TaskState))]
        public TaskState State { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
