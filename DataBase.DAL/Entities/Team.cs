using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataBase.DAL.Entities
{
    public sealed class Team : IEntity
    {
        public Team()
        {
            CreatedAt = DateTime.UtcNow;
            Users = new List<User>();
            Projects = new List<Project>();
        }

        public int Id { get; set; }

        [Required]
        [MinLength(5)]
        [MaxLength(200)]
        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }

        public IEnumerable<User> Users { get; set; }
        public IEnumerable<Project> Projects { get; set; }
    }
}
