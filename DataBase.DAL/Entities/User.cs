using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataBase.DAL.Entities
{
    public sealed class User : IEntity
    {
        public User()
        {
            RegisteredAt = DateTime.UtcNow;
            Projects = new List<Project>();
            Tasks = new List<TaskModel>();
        }

        public int Id { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [MinLength(2)]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [MinLength(2)]
        [MaxLength(50)]
        public string LastName { get; set; }

        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }

        public IEnumerable<Project> Projects { get; set; }
        public IEnumerable<TaskModel> Tasks { get; set; }
    }
}
