using System;
using Microsoft.EntityFrameworkCore;
using DataBase.DAL.Entities;

namespace DataBase.DAL.Context
{
    public sealed class DatabaseContext : DbContext
    {
        public DatabaseContext() : base() { }
        public DatabaseContext(DbContextOptions<DatabaseContext> opt) : base(opt) { }

        public DbSet<User> Users { get; set; }
        public DbSet<TaskModel> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Project> Projects { get; set; }

        protected override void OnModelCreating(ModelBuilder opt)
        {
            opt.ConfigureModels();
            opt.Seed();

            base.OnModelCreating(opt);
        }

        /* Temporary uncomment if `dotnet ef <something>` fails */
        /*
        protected override void OnConfiguring(DbContextOptionsBuilder opt)
        {
            if (!opt.IsConfigured)
            {
                opt.UseNpgsql(Environment.GetEnvironmentVariable("DB_CONNECTION_STRING"));
            }
        }
        */
    }
}
