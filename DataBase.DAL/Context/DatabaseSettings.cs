using DataBase.DAL.Seeds;

namespace DataBase.DAL.Context
{
    internal static class DatabaseSettings
    {
        public static readonly int usersSeedCount = 130;
        public static readonly int tasksSeedCount = 520;
        public static readonly int teamsSeedCount = 10;
        public static readonly int projectsSeedCount = 120;

        public static readonly ISeeder seeder = new DefaultSeeder();
    }
}
