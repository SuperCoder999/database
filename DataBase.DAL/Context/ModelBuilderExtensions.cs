using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using DataBase.DAL.Entities;

namespace DataBase.DAL.Context
{
    internal static class ModelBuilderExtensions
    {
        public static void ConfigureModels(this ModelBuilder opt)
        {
            opt.Entity<User>()
                .HasOne(u => u.Team)
                .WithMany(t => t.Users)
                .HasForeignKey(u => u.TeamId)
                .OnDelete(DeleteBehavior.SetNull);

            opt.Entity<TaskModel>()
                .HasOne(t => t.Performer)
                .WithMany(u => u.Tasks)
                .HasForeignKey(t => t.PerformerId)
                .OnDelete(DeleteBehavior.SetNull);

            opt.Entity<TaskModel>()
                .HasOne(t => t.Project)
                .WithMany(p => p.Tasks)
                .HasForeignKey(t => t.ProjectId)
                .OnDelete(DeleteBehavior.Cascade);

            opt.Entity<Project>()
                .HasOne(p => p.Author)
                .WithMany(u => u.Projects)
                .HasForeignKey(p => p.AuthorId)
                .OnDelete(DeleteBehavior.SetNull);

            opt.Entity<Project>()
                .HasOne(p => p.Team)
                .WithMany(t => t.Projects)
                .HasForeignKey(p => p.TeamId)
                .OnDelete(DeleteBehavior.SetNull);
        }

        public static void Seed(this ModelBuilder opt)
        {
            IEnumerable<Team> teams =
                DatabaseSettings.seeder.GetTeams(DatabaseSettings.teamsSeedCount);

            IEnumerable<User> users =
                DatabaseSettings.seeder.GetUsers(DatabaseSettings.usersSeedCount, teams);

            IEnumerable<Project> projects =
                DatabaseSettings.seeder.GetProjects(DatabaseSettings.projectsSeedCount, users, teams);

            IEnumerable<TaskModel> tasks =
                DatabaseSettings.seeder.GetTasks(DatabaseSettings.tasksSeedCount, users, projects);

            opt.Entity<User>().HasData(users);
            opt.Entity<TaskModel>().HasData(tasks);
            opt.Entity<Team>().HasData(teams);
            opt.Entity<Project>().HasData(projects);
        }
    }
}
