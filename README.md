# DataBase

## Requirements:

- .NET 5.0
- Docker
- docker-compose

## Setup:

- Create `.env` and `.database.env` based on `.env.example` and `.database.env.example`
- `docker-compose up -d`

## Start:

### Backend:

- `cd DataBase.WebAPI`
- `dotnet run`

### Client:

- `cd DataBase.Client`
- `dotnet run`

## DB operations:

- `cd DataBase.DAL`
- `dotnet ef <something> --startup-project ../DataBase.WebAPI`

### If `dotnet ef <something> --startup-project ../DataBase.WebAPI` fails (can't connect to DB, etc.)

Uncomment marked lines in `DataBase.DAL/Context/DatabaseContext.cs`

## Notes:

- All important DB things (seeders, migrations, etc.) are located at `DataBase.DAL`
- Business logic is located at `DataBase.BLL`
- Splitted queries increase performance in **8** times
- PostgreSQL is used, because it's faster
- First DB queries are automatically cached, so may last more time and load more data

## Important!

- See partial update logic in `DataBase.WebAPI/Controllers/AbstractController.cs`. If you have any advice, how to simplify it, please write.
