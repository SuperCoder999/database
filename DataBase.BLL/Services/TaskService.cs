using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using FluentValidation;
using DataBase.Common.DTO.Task;
using DataBase.Common.Exceptions;
using DataBase.DAL.Entities;
using DataBase.BLL.Interfaces;
using DataBase.DAL.Context;

namespace DataBase.BLL.Services
{
    public sealed class TaskService : AbstractService<TaskModel, TaskDTO, CreateTaskDTO, UpdateTaskDTO>, ITaskService
    {
        private readonly IProjectService _projectService;
        private readonly IUserService _userService;

        public TaskService(
            DatabaseContext context,
            IMapper mapper,
            IValidator<CreateTaskDTO> createValidator,
            IValidator<UpdateTaskDTO> updateValidator,
            IProjectService projectService,
            IUserService userService
        ) : base(context, mapper, createValidator, updateValidator, ctx => ctx.Tasks)
        {
            _projectService = projectService;
            _userService = userService;
        }

        public async Task<IEnumerable<TaskDTO>> GetAssignedToWithShortName(int userId)
        {
            bool userExists = await _userService.Exists(userId);

            if (!userExists)
            {
                throw new NotFoundException("User");
            }

            return IncludeRelations(_context.Tasks)
                .Where(t => t.Name.Length < 45 && t.PerformerId == userId)
                .Select(_mapper.Map<TaskModel, TaskDTO>);
        }

        public async Task<IEnumerable<TaskShortDTO>> GetShortFinishedInCurrentYearAssignedTo(int userId)
        {
            bool userExists = await _userService.Exists(userId);

            if (!userExists)
            {
                throw new NotFoundException("User");
            }

            return _context.Tasks
                .Where(t =>
                    t.FinishedAt.HasValue &&
                    t.FinishedAt.Value.Year == DateTime.UtcNow.Year &&
                    t.PerformerId == userId)
                .Select(_mapper.Map<TaskModel, TaskShortDTO>);
        }

        protected override IQueryable<TaskModel> IncludeRelations(IQueryable<TaskModel> queryable)
        {
            return queryable.Include(t => t.Performer);
        }

        protected override async Task CheckRelations(TaskModel entity)
        {
            bool projectExists = await _projectService.Exists(entity.ProjectId);

            if (!projectExists)
            {
                throw new NotFoundException("Project");
            }

            if (entity.PerformerId != null)
            {
                bool performerExists = await _userService.Exists(entity.PerformerId.Value);

                if (!performerExists)
                {
                    throw new NotFoundException("Performer");
                }
            }
        }
    }
}
