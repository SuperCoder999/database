using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using FluentValidation;
using DataBase.Common.DTO.User;
using DataBase.Common.DTO.Task;
using DataBase.Common.DTO.Project;
using DataBase.Common.Exceptions;
using DataBase.DAL.Entities;
using DataBase.BLL.Interfaces;
using DataBase.DAL.Context;
using System.Collections.Generic;

namespace DataBase.BLL.Services
{
    public sealed class UserService : AbstractService<User, UserDTO, CreateUserDTO, UpdateUserDTO>, IUserService
    {
        private readonly ITeamService _teamService;

        public UserService(
            DatabaseContext context,
            IMapper mapper,
            IValidator<CreateUserDTO> createValidator,
            IValidator<UpdateUserDTO> updateValidator,
            ITeamService teamService
        ) : base(context, mapper, createValidator, updateValidator, ctx => ctx.Users)
        {
            _teamService = teamService;
        }

        public async Task<IEnumerable<UserWithTasksDTO>> GetWithTasksOrderedByFirstName()
        {
            return await Task.Run(() =>
                _context.Users
                    .Include(u => u.Tasks.OrderByDescending(t => t.Name.Length))
                    .OrderBy(u => u.FirstName)
                    .Select(_mapper.Map<User, UserWithTasksDTO>)
            );
        }

        public async Task<UserAdditionalInfoDTO> GetAdditionalInfo(int id)
        {
            // This splitting increases performance in 8 and more times
            User user = await _context.Users
                .Where(u => u.Id == id)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                throw new NotFoundException();
            }

            Project lastProject = await _context.Projects
                .AsSplitQuery()
                .Where(p => p.AuthorId == id)
                .Include(p => p.Team)
                    .ThenInclude(t => t.Users)
                .Include(p => p.Tasks)
                    .ThenInclude(t => t.Performer)
                .OrderByDescending(p => p.CreatedAt)
                .FirstOrDefaultAsync();

            TaskModel longestTask = await _context.Tasks
                .Where(t => t.PerformerId == id && t.FinishedAt.HasValue)
                .OrderByDescending(t => t.FinishedAt - t.CreatedAt)
                .FirstOrDefaultAsync();

            return new UserAdditionalInfoDTO
            {
                User = _mapper.Map<User, UserDTO>(user),
                LastProject = lastProject != null ? _mapper.Map<Project, ProjectDTO>(lastProject) : null,
                LastProjectTasksCount = lastProject != null ? lastProject.Tasks.Count() : 0,
                UnfinishedTasksCount = _context.Tasks
                    .Where(t => t.PerformerId == id && !t.FinishedAt.HasValue)
                    .Count(),
                LongestTask = longestTask != null ? _mapper.Map<TaskModel, TaskWithoutPerformerDTO>(longestTask) : null
            };
        }

        protected override async Task CheckRelations(User entity)
        {
            if (entity.TeamId != null)
            {
                bool teamExists = await _teamService.Exists(entity.TeamId.Value);

                if (!teamExists)
                {
                    throw new NotFoundException("Team");
                }
            }
        }
    }
}
