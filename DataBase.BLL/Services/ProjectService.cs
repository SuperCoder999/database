using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using FluentValidation;
using DataBase.Common.DTO.Task;
using DataBase.Common.DTO.Project;
using DataBase.Common.Exceptions;
using DataBase.DAL.Entities;
using DataBase.BLL.Interfaces;
using DataBase.DAL.Context;

namespace DataBase.BLL.Services
{
    public sealed class ProjectService : AbstractService<Project, ProjectDTO, CreateProjectDTO, UpdateProjectDTO>, IProjectService
    {
        private readonly ITeamService _teamService;
        private readonly IUserService _userService;

        public ProjectService(
            DatabaseContext context,
            IMapper mapper,
            IValidator<CreateProjectDTO> createValidator,
            IValidator<UpdateProjectDTO> updateValidator,
            ITeamService teamService,
            IUserService userService
        ) : base(context, mapper, createValidator, updateValidator, ctx => ctx.Projects)
        {
            _teamService = teamService;
            _userService = userService;
        }

        public async Task<IEnumerable<ProjectWithTasksCountDTO>> GetWithTasksCount(int userId)
        {
            bool userExists = await _userService.Exists(userId);

            if (!userExists)
            {
                throw new NotFoundException("User");
            }

            // Tasks count is handled by mapper (it is simple enough)

            return IncludeRelations(_context.Projects)
                .Where(p => p.AuthorId == userId)
                .Select(_mapper.Map<Project, ProjectWithTasksCountDTO>);
        }

        public async Task<ProjectAdditionalInfoDTO> GetAdditionalInfo(int id)
        {
            // This splitting increases performance in 8 and more times
            Project project = await IncludeRelations(_context.Projects)
                .Where(p => p.Id == id)
                .FirstOrDefaultAsync();

            if (project == null)
            {
                throw new NotFoundException();
            }

            TaskModel shortestTask = await _context.Tasks
                .Where(t => t.ProjectId == id)
                .Include(t => t.Performer)
                .OrderBy(t => t.Name.Length)
                .FirstOrDefaultAsync();

            TaskModel longestTask = await _context.Tasks
                .Where(t => t.ProjectId == id)
                .Include(t => t.Performer)
                .OrderByDescending(t => t.Description.Length)
                .FirstOrDefaultAsync();

            return new ProjectAdditionalInfoDTO
            {
                Project = _mapper.Map<Project, ProjectDTO>(project),
                ShortestNameTask = shortestTask != null ? _mapper.Map<TaskModel, TaskDTO>(shortestTask) : null,
                LongestDescriptionTask = longestTask != null ? _mapper.Map<TaskModel, TaskDTO>(longestTask) : null,
                UsersCountInTeam = project.Description.Length > 20 || project.Tasks.Count() < 3
                    ? project.Team.Users.Count()
                    : null
            };
        }

        protected override IQueryable<Project> IncludeRelations(IQueryable<Project> queryable)
        {
            return queryable
                .Include(p => p.Author)
                .Include(p => p.Team)
                    .ThenInclude(t => t.Users)
                .Include(p => p.Tasks)
                    .ThenInclude(t => t.Performer);
        }

        protected override async Task CheckRelations(Project entity)
        {
            if (entity.TeamId != null)
            {
                bool teamExists = await _teamService.Exists(entity.TeamId.Value);

                if (!teamExists)
                {
                    throw new NotFoundException("Team");
                }
            }

            if (entity.AuthorId != null)
            {
                bool authorExists = await _userService.Exists(entity.AuthorId.Value);

                if (!authorExists)
                {
                    throw new NotFoundException("Author");
                }
            }
        }
    }
}
