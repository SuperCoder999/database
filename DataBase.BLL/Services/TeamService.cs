using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using FluentValidation;
using DataBase.Common.DTO.Team;
using DataBase.DAL.Entities;
using DataBase.BLL.Interfaces;
using DataBase.DAL.Context;

namespace DataBase.BLL.Services
{
    public sealed class TeamService : AbstractService<Team, TeamDTO, CreateTeamDTO, UpdateTeamDTO>, ITeamService
    {
        public TeamService(
            DatabaseContext context,
            IMapper mapper,
            IValidator<CreateTeamDTO> createValidator,
            IValidator<UpdateTeamDTO> updateValidator
        ) : base(context, mapper, createValidator, updateValidator, ctx => ctx.Teams) { }

        public async Task<IEnumerable<TeamShortDTO>> GetShortWithUsersOlder10()
        {
            return await Task.Run(() =>
                IncludeRelations(_context.Teams)
                    .Where(t => t.Users.All(u => DateTime.UtcNow.Year - u.BirthDay.Year > 10) && t.Users.Count() > 0)
                    .Select(t => new Team
                    {
                        Id = t.Id,
                        Name = t.Name,
                        CreatedAt = t.CreatedAt,
                        Users = t.Users.OrderByDescending(u => u.RegisteredAt).AsEnumerable()
                    })
                    .Select(_mapper.Map<Team, TeamShortDTO>)
            );
        }

        protected override IQueryable<Team> IncludeRelations(IQueryable<Team> queryable)
        {
            return queryable.Include(t => t.Users);
        }
    }
}
