using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using FluentValidation;
using Newtonsoft.Json;
using DataBase.DAL.Context;
using DataBase.DAL.Entities;
using DataBase.BLL.Interfaces;
using DataBase.Common;
using DataBase.Common.Exceptions;

namespace DataBase.BLL.Services
{
    public abstract class AbstractService<TEntity, TDTO, TCreateDTO, TUpdateDTO>
        : IService<TEntity, TDTO, TCreateDTO>
        where TEntity : class, IEntity, new()
        where TDTO : struct
        where TCreateDTO : struct
    {
        protected readonly DatabaseContext _context;
        protected readonly IMapper _mapper;
        protected readonly IValidator<TCreateDTO> _createValidator;
        protected readonly IValidator<TUpdateDTO> _updateValidator;
        private readonly Func<DbSet<TEntity>> _GetDBSet;

        public AbstractService(
            DatabaseContext context,
            IMapper mapper,
            IValidator<TCreateDTO> createValidator,
            IValidator<TUpdateDTO> updateValidator,
            Func<DatabaseContext, DbSet<TEntity>> DBSetSelector
        )
        {
            _context = context;
            _mapper = mapper;
            _createValidator = createValidator;
            _updateValidator = updateValidator;
            _GetDBSet = () => DBSetSelector(context);
        }

        public virtual async Task<IEnumerable<TDTO>> Get()
        {
            return await Task.Run(() =>
                IncludeRelations(_GetDBSet())
                    .Select(_mapper.Map<TEntity, TDTO>)
            );
        }

        public virtual async Task<TDTO> Get(int id)
        {
            TEntity entity = await IncludeRelations(_GetDBSet())
                .Where(e => e.Id == id)
                .FirstOrDefaultAsync();

            if (entity == null)
            {
                throw new NotFoundException();
            }

            return _mapper.Map<TEntity, TDTO>(entity);
        }

        public virtual async Task<TEntity> GetRaw(int id)
        {
            TEntity entity = await _GetDBSet()
                .Where(e => e.Id == id)
                .FirstOrDefaultAsync();

            if (entity == null)
            {
                throw new NotFoundException();
            }

            return entity;
        }

        public virtual async Task<bool> Exists(int id)
        {
            return await _GetDBSet()
                .AnyAsync(e => e.Id == id);
        }

        public virtual async Task<TDTO> Create(TCreateDTO data)
        {
            await _createValidator.ValidateAndThrowAsync(data);
            TEntity entity = _mapper.Map<TCreateDTO, TEntity>(data);

            await CheckRelations(entity);
            _GetDBSet().Add(entity);
            await _context.SaveChangesAsync();

            return await Get(entity.Id);
        }

        public virtual async Task<TDTO> Update(int id, TEntity data)
        //                                             ^^^^^^^ Custom logic
        {
            await CheckRelations(data);
            _GetDBSet().Update(data);
            await _context.SaveChangesAsync();

            return await Get(id);
        }

        public virtual async Task<TEntity> RepopulateUpdateRequest(string stringBody, TEntity existing)
        {
            TUpdateDTO oldAsDTO = _mapper.Map<TEntity, TUpdateDTO>(existing);
            object oldAsObject = (object)oldAsDTO;

            JsonConvert.PopulateObject(stringBody, oldAsObject);
            TUpdateDTO newAsDTO = (TUpdateDTO)oldAsObject;

            await _updateValidator.ValidateAndThrowAsync(newAsDTO);
            Utils.PopulateCSharpObject(newAsDTO, ref existing);

            return existing;
        }

        public virtual async Task Delete(int id)
        {
            bool exists = await Exists(id);

            if (!exists)
            {
                throw new NotFoundException();
            }

            _GetDBSet().Remove(new TEntity { Id = id });
            await _context.SaveChangesAsync();
        }

        protected virtual IQueryable<TEntity> IncludeRelations(IQueryable<TEntity> queryable) => queryable;
        protected virtual Task CheckRelations(TEntity entity) => Task.Run(() => { });
    }
}
