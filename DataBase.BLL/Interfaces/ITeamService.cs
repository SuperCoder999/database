using System.Threading.Tasks;
using System.Collections.Generic;
using DataBase.Common.DTO.Team;
using DataBase.DAL.Entities;

namespace DataBase.BLL.Interfaces
{
    public interface ITeamService : IService<Team, TeamDTO, CreateTeamDTO>
    {
        Task<IEnumerable<TeamShortDTO>> GetShortWithUsersOlder10();
    }
}
