using System.Threading.Tasks;
using System.Collections.Generic;
using DataBase.Common.DTO.Task;
using DataBase.DAL.Entities;

namespace DataBase.BLL.Interfaces
{
    public interface ITaskService : IService<TaskModel, TaskDTO, CreateTaskDTO>
    {
        Task<IEnumerable<TaskDTO>> GetAssignedToWithShortName(int userId);
        Task<IEnumerable<TaskShortDTO>> GetShortFinishedInCurrentYearAssignedTo(int userId);
    }
}
