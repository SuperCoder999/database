using System.Collections.Generic;
using System.Threading.Tasks;
using DataBase.DAL.Entities;

namespace DataBase.BLL.Interfaces
{
    public interface IService<TEntity, TDTO, TCreateDTO>
        where TEntity : class, IEntity, new()
        where TDTO : struct
        where TCreateDTO : struct
    {
        Task<IEnumerable<TDTO>> Get();
        Task<TDTO> Get(int id);
        Task<TEntity> GetRaw(int id);
        Task<bool> Exists(int id);
        Task<TDTO> Create(TCreateDTO data);
        Task<TDTO> Update(int id, TEntity data);
        Task<TEntity> RepopulateUpdateRequest(string stringBody, TEntity existing);
        Task Delete(int id);
    }
}
