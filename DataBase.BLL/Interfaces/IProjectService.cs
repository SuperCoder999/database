using System.Threading.Tasks;
using System.Collections.Generic;
using DataBase.Common.DTO.Project;
using DataBase.DAL.Entities;

namespace DataBase.BLL.Interfaces
{
    public interface IProjectService : IService<Project, ProjectDTO, CreateProjectDTO>
    {
        Task<IEnumerable<ProjectWithTasksCountDTO>> GetWithTasksCount(int userId);
        Task<ProjectAdditionalInfoDTO> GetAdditionalInfo(int id);
    }
}
