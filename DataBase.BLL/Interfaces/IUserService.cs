using System.Threading.Tasks;
using System.Collections.Generic;
using DataBase.Common.DTO.User;
using DataBase.DAL.Entities;

namespace DataBase.BLL.Interfaces
{
    public interface IUserService : IService<User, UserDTO, CreateUserDTO>
    {
        Task<IEnumerable<UserWithTasksDTO>> GetWithTasksOrderedByFirstName();
        Task<UserAdditionalInfoDTO> GetAdditionalInfo(int id);
    }
}
