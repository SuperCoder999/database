using FluentValidation;
using DataBase.Common.DTO.Project;

namespace DataBase.BLL.Validators.Project
{
    public sealed class UpdateProjectValidator : AbstractValidator<UpdateProjectDTO>
    {
        public UpdateProjectValidator()
        {
            When(
                p => p.Name != null,
                () =>
                {
                    RuleFor(p => p.Name.Trim())
                        .NotEmpty()
                        .WithMessage("Name must not be empty");

                    RuleFor(p => p.Name.Trim())
                        .Length(5, 200)
                        .WithMessage("Name must be from 5 to 200 characters");
                }
            );

            When(
                p => p.Description != null,
                () =>
                {
                    RuleFor(p => p.Description.Trim())
                        .NotEmpty()
                        .WithMessage("Description must not be empty");

                    RuleFor(p => p.Description.Trim())
                        .Length(2, 2000)
                        .WithMessage("Description must be from 2 to 2000 characters");
                }
            );

            When(
                p => p.Deadline != null,
                () => RuleFor(p => p.Deadline)
                    .NotEmpty()
                    .WithMessage("Deadline must not be empty")
            );
        }
    }
}
