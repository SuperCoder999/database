using System;
using FluentValidation;
using DataBase.Common.DTO.Task;

namespace DataBase.BLL.Validators.Task
{
    public sealed class UpdateTaskValidator : AbstractValidator<UpdateTaskDTO>
    {
        public UpdateTaskValidator()
        {
            When(
                t => t.Name != null,
                () =>
                {
                    RuleFor(t => t.Name.Trim())
                        .NotEmpty()
                        .WithMessage("Name must not be empty");

                    RuleFor(t => t.Name.Trim())
                        .Length(5, 200)
                        .WithMessage("Name must be from 5 to 200 characters");
                }
            );

            When(
                t => t.Description != null,
                () =>
                {
                    RuleFor(t => t.Description.Trim())
                        .NotEmpty()
                        .WithMessage("Description must not be empty");

                    RuleFor(t => t.Description.Trim())
                        .Length(2, 700)
                        .WithMessage("Description must be from 2 to 700 characters");
                }
            );

            When(
                t => t.State != null,
                () => RuleFor(t => t.State)
                    .NotEmpty()
                    .IsInEnum()
                    .WithMessage("State must be from 0 to 3")
            );

            When(
                t => t.FinishedAt != null,
                () => RuleFor(t => t.FinishedAt)
                    .NotEmpty()
                    .LessThan(t => DateTime.UtcNow)
                    .WithMessage("Finish date must be earlier than now")
            );
        }
    }
}
