using FluentValidation;
using DataBase.Common.DTO.Team;

namespace DataBase.BLL.Validators.Team
{
    public sealed class UpdateTeamValidator : AbstractValidator<UpdateTeamDTO>
    {
        public UpdateTeamValidator()
        {
            When(
                t => t.Name != null,
                () =>
                {
                    RuleFor(t => t.Name)
                        .NotEmpty()
                        .WithMessage("Name must not be empty");

                    RuleFor(t => t.Name)
                        .Length(5, 200)
                        .WithMessage("Name must be from 5 to 200 characters");
                }
            );
        }
    }
}
