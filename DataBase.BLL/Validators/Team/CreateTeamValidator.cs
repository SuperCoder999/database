using FluentValidation;
using DataBase.Common.DTO.Team;

namespace DataBase.BLL.Validators.Team
{
    public sealed class CreateTeamValidator : AbstractValidator<CreateTeamDTO>
    {
        public CreateTeamValidator()
        {
            RuleFor(t => t.Name.Trim())
                .NotEmpty()
                .WithMessage("Name must not be empty");

            RuleFor(t => t.Name.Trim())
                .Length(5, 200)
                .WithMessage("Name must be from 5 to 200 characters");
        }
    }
}
