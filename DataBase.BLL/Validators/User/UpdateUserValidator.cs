using System;
using FluentValidation;
using DataBase.Common.DTO.User;

namespace DataBase.BLL.Validators.User
{
    public sealed class UpdateUserValidator : AbstractValidator<UpdateUserDTO>
    {
        public UpdateUserValidator()
        {
            When(
                u => u.Email != null,
                () => RuleFor(u => u.Email.Trim())
                    .EmailAddress()
                    .WithMessage("Email address must be valid")
            );

            When(
                u => u.FirstName != null,
                () =>
                {
                    RuleFor(u => u.FirstName.Trim())
                        .NotEmpty()
                        .WithMessage("First name must not be empty");

                    RuleFor(u => u.FirstName.Trim())
                        .Length(2, 50)
                        .WithMessage("First name must be from 2 to 50 characters");
                }
            );

            When(
                u => u.LastName != null,
                () =>
                {
                    RuleFor(u => u.LastName.Trim())
                        .NotEmpty()
                        .WithMessage("Last name must not be empty");

                    RuleFor(u => u.LastName.Trim())
                        .Length(2, 50)
                        .WithMessage("Last name must be from 2 to 50 characters");
                }
            );

            When(
                u => u.BirthDay != null,
                () => RuleFor(u => u.BirthDay)
                    .NotEmpty()
                    .LessThan(u => DateTime.UtcNow)
                    .WithMessage("Birth day must be earlier than now")
            );
        }
    }
}
