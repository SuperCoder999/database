using AutoMapper;
using DataBase.DAL.Entities;
using DataBase.Common.DTO.Team;

namespace DataBase.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<CreateTeamDTO, Team>();
            CreateMap<Team, UpdateTeamDTO>();
            CreateMap<Team, TeamDTO>();
            CreateMap<Team, TeamShortDTO>();
        }
    }
}
