using System.Linq;
using AutoMapper;
using DataBase.DAL.Entities;
using DataBase.Common.DTO.Project;

namespace DataBase.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<CreateProjectDTO, Project>();
            CreateMap<Project, UpdateProjectDTO>();
            CreateMap<Project, ProjectDTO>();

            CreateMap<Project, ProjectWithTasksCountDTO>()
                .ForMember<int>(dto => dto.TasksCount, opt => opt.MapFrom(p => p.Tasks.Count()));
        }
    }
}
