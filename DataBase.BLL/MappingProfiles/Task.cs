using AutoMapper;
using DataBase.DAL.Entities;
using DataBase.Common.DTO.Task;

namespace DataBase.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<CreateTaskDTO, TaskModel>();
            CreateMap<TaskModel, UpdateTaskDTO>();
            CreateMap<TaskModel, TaskDTO>();
            CreateMap<TaskModel, TaskWithoutPerformerDTO>();
            CreateMap<TaskModel, TaskShortDTO>();
        }
    }
}
