using AutoMapper;
using DataBase.DAL.Entities;
using DataBase.Common.DTO.User;

namespace DataBase.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<CreateUserDTO, User>();
            CreateMap<User, UpdateUserDTO>();
            CreateMap<User, UserDTO>();
            CreateMap<User, UserWithTasksDTO>();
        }
    }
}
