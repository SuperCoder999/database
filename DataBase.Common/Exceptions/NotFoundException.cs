using System;

namespace DataBase.Common.Exceptions
{
    public sealed class NotFoundException : Exception
    {
        public NotFoundException() : base("Not found") { }
        public NotFoundException(string entity) : base($"{entity} is not found") { }
    }
}
