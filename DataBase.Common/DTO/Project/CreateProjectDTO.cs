using System;
using Newtonsoft.Json;

namespace DataBase.Common.DTO.Project
{
    public struct CreateProjectDTO
    {
        public int? AuthorId { get; set; }
        public int? TeamId { get; set; }
        [JsonRequired] public string Name { get; set; }
        public string Description { get; set; }
        [JsonRequired] public DateTime Deadline { get; set; }
    }
}
