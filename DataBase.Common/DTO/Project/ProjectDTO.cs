using System;
using System.Collections.Generic;
using DataBase.Common.DTO.Team;
using DataBase.Common.DTO.Task;
using DataBase.Common.DTO.User;

namespace DataBase.Common.DTO.Project
{
    public struct ProjectDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public IEnumerable<TaskDTO> Tasks;
        public TeamDTO? Team { get; set; }
        public UserDTO? Author { get; set; }
    }
}
