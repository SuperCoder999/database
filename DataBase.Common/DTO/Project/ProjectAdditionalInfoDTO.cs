using DataBase.Common.DTO.Task;

namespace DataBase.Common.DTO.Project
{
    public struct ProjectAdditionalInfoDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO? LongestDescriptionTask { get; set; }
        public TaskDTO? ShortestNameTask { get; set; }
        public int? UsersCountInTeam { get; set; }
    }
}
