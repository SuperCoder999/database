using System;
using DataBase.Common.Enums;
using DataBase.Common.DTO.User;

namespace DataBase.Common.DTO.Task
{
    public struct TaskDTO
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public UserDTO? Performer { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
