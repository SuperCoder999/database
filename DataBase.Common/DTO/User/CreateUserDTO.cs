using System;
using Newtonsoft.Json;

namespace DataBase.Common.DTO.User
{
    public struct CreateUserDTO
    {
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [JsonRequired] public string Email { get; set; }
        [JsonRequired] public DateTime BirthDay { get; set; }
    }
}
