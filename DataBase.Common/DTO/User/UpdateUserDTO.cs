#nullable enable

using System;

namespace DataBase.Common.DTO.User
{
    public struct UpdateUserDTO
    {
        public int? TeamId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTime? BirthDay { get; set; }
    }
}
