#nullable enable

namespace DataBase.Common.DTO.Team
{
    public struct UpdateTeamDTO
    {
        public string? Name { get; set; }
    }
}
