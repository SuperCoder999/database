using Newtonsoft.Json;

namespace DataBase.Common.DTO.Team
{
    public struct CreateTeamDTO
    {
        [JsonRequired] public string Name { get; set; }
    }
}
