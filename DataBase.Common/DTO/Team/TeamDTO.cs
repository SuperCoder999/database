using System;
using System.Collections.Generic;
using DataBase.Common.DTO.User;

namespace DataBase.Common.DTO.Team
{
    public struct TeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public IEnumerable<UserDTO> Users;
    }
}
