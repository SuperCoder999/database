using System.Collections.Generic;

namespace DataBase.Common.DTO
{
    public struct HttpErrorDTO
    {
        public string Error { get; set; }
    }
}
