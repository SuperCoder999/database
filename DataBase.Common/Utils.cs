using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DataBase.Common
{
    public static class Utils
    {
        public static TAttr GetAttribute<T, TAttr>(bool inherit = false)
            where T : class
            where TAttr : Attribute
        {
            TAttr attribute = typeof(T).GetCustomAttribute<TAttr>(inherit);

            return attribute;
        }

        public static TAttr GetAttribute<TAttr>(Type type, bool inherit = false) where TAttr : Attribute
        {
            TAttr attribute = type.GetCustomAttribute<TAttr>(inherit);

            return attribute;
        }

        public static List<TAttr> GetAttributes<T, TAttr>(bool inherit = false)
            where T : class
            where TAttr : Attribute
        {
            IEnumerable<TAttr> attributes = typeof(T).GetCustomAttributes<TAttr>(inherit);

            return attributes.ToList();
        }

        public static List<TAttr> GetAttributes<TAttr>(Type type, bool inherit = false) where TAttr : Attribute
        {
            IEnumerable<TAttr> attributes = type.GetCustomAttributes<TAttr>(inherit);

            return attributes.ToList();
        }

        public static T DeserializeJson<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static void PopulateCSharpObject<TFrom, TTo>(TFrom from, ref TTo to)
        {
            PropertyInfo[] fromProps = from.GetType().GetProperties();
            PropertyInfo[] toProps = to.GetType().GetProperties();

            foreach (PropertyInfo fromProp in fromProps)
            {
                foreach (PropertyInfo toProp in toProps)
                {
                    if (fromProp.Name == toProp.Name && fromProp.PropertyType == toProp.PropertyType)
                    {
                        toProp.SetValue(to, fromProp.GetValue(from));
                        break;
                    }
                }
            }
        }
    }
}
